package ma.nemo.assignment.services;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public void addProduct(Product product) {
        if (product.getQuantityInStock() <= 500) {
            productRepository.save(product);
        } else {
            throw new IllegalArgumentException("Quantity must be under 500 units");
        }
    }

    public Product sellProduct(String productCode, Integer quantity) {
        Product product = productRepository.findByProductCode(productCode);

        if (product == null) {
            throw new IllegalArgumentException("Product not found");
        }

        if (product.getQuantityInStock() >= quantity) {
            product.setQuantityInStock(product.getQuantityInStock() - quantity);
            productRepository.save(product);

            Product soldProduct = new Product();
            soldProduct.setProductCode(product.getProductCode());
            soldProduct.setQuantityInStock(quantity);

            return soldProduct;
        } else {
            throw new IllegalArgumentException("Not enough quantity in stock");
        }
    }

    public Product getProductByCode(String productCode) {
        return productRepository.findByProductCode(productCode);
    }

    public void updateQuantity(String productCode, int quantityChange) {
        Product product = productRepository.findByProductCode(productCode);

        if (product != null) {
            product.setQuantityInStock(product.getQuantityInStock() + quantityChange);
            productRepository.save(product);
        }
    }

    public void returnProduct(String productCode, Integer returnQuantity, String returnReason) {
        Product product = productRepository.findByProductCode(productCode);

        if (product == null) {
            throw new IllegalArgumentException("Product not found");
        }

        product.setQuantityInStock(product.getQuantityInStock() + returnQuantity);
        productRepository.save(product);
    }
}
