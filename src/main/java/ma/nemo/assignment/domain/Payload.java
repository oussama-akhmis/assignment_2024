package ma.nemo.assignment.domain;

import java.util.Date;

public class Payload {
    public String errorMessage;

    public String productCode;
    public Integer quantityInStock;
    public Date expirationDate = new Date();

    public Payload(){

    }

    public Payload(String productCode, Integer quantityInStock, Date expirationDate, String errorMessage) {
        this.productCode = productCode;
        this.quantityInStock = quantityInStock;
        this.expirationDate = expirationDate;
        this.errorMessage = errorMessage;
    }



    public String getErrorMessage() {
     return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getProductCode() {
        return productCode;
    }

    public Integer getQuantityInStock() {
        return quantityInStock;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setQuantityInStock(Integer quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}
