package ma.nemo.assignment.domain;

public class SalePayload {
    private String message;
    private String productCode;
    private Integer soldQuantity;

    public SalePayload() {
    }

    public SalePayload(String message, String productCode, Integer soldQuantity) {
        this.message = message;
        this.productCode = productCode;
        this.soldQuantity = soldQuantity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(Integer soldQuantity) {
        this.soldQuantity = soldQuantity;
    }
}
