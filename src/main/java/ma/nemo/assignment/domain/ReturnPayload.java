package ma.nemo.assignment.domain;

public class ReturnPayload {
    private String message;

    private String productCode;
    private Integer returnQuantity;
    private String returnReason;


    public ReturnPayload(String productCode, Integer returnQuantity, String returnReason, String message) {
        this.productCode = productCode;
        this.returnQuantity = returnQuantity;
        this.returnReason = returnReason;
        this.message = message;
    }

    public ReturnPayload() {

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(Integer returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }
}
