package ma.nemo.assignment.controller;

import ma.nemo.assignment.domain.Payload;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.services.ProductService;
import org.apache.logging.log4j.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SupplyController {

    @Autowired
    ProductService productService;

    @PostMapping("/supply")
    public Payload addNewProduct(@RequestBody Product product) {
        Payload payload = new Payload();

        try {
            // Check if the product already exists
            Product existingProduct = productService.getProductByCode(product.getProductCode());

            // If the product doesn't exist, add it as is
            if (existingProduct == null) {
                productService.addProduct(product);
                payload.setProductCode(product.getProductCode());
                payload.setQuantityInStock(product.getQuantityInStock());
                payload.setExpirationDate(product.getExpirationDate());
            } else {
                // If the product exists, do not change the quantity
                payload.setProductCode(existingProduct.getProductCode());
                payload.setQuantityInStock(existingProduct.getQuantityInStock());
                payload.setExpirationDate(existingProduct.getExpirationDate());
            }
        } catch (IllegalArgumentException e) {
            payload.setErrorMessage("Error: " + e.getMessage());
            payload.setProductCode(null);
            payload.setQuantityInStock(null);
            payload.setExpirationDate(null);
        }
        return payload;
    }
}


