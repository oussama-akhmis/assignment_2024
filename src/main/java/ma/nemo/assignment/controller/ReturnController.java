package ma.nemo.assignment.controller;

import ma.nemo.assignment.domain.ReturnPayload;
import ma.nemo.assignment.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ReturnController {

    @Autowired
    ProductService productService;

    @PostMapping("/return")
    public ReturnPayload returnProduct(@RequestBody ReturnPayload returnPayload) {
        ReturnPayload payload = new ReturnPayload();

        try {
            productService.returnProduct(returnPayload.getProductCode(), returnPayload.getReturnQuantity(), returnPayload.getReturnReason());
            payload.setMessage("Product returned successfully");
            payload.setProductCode(returnPayload.getProductCode());
            payload.setReturnQuantity(returnPayload.getReturnQuantity());
            payload.setReturnReason(returnPayload.getReturnReason());
        } catch (IllegalArgumentException e) {
            payload.setMessage("Error: " + e.getMessage());
        }

        return payload;
    }
}

