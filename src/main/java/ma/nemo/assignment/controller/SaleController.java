package ma.nemo.assignment.controller;

import ma.nemo.assignment.domain.SalePayload;
import ma.nemo.assignment.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SaleController {

    @Autowired
    ProductService productService;

    @PostMapping("/sale")
    public SalePayload sellProduct(@RequestBody SalePayload salePayload) {
        SalePayload payload = new SalePayload();

        try {
            productService.sellProduct(salePayload.getProductCode(), salePayload.getSoldQuantity());
            payload.setMessage("Product sold successfully");
            payload.setProductCode(salePayload.getProductCode());
            payload.setSoldQuantity(salePayload.getSoldQuantity());
        } catch (IllegalArgumentException e) {
            payload.setMessage("Error: " + e.getMessage());
        }

        return payload;
    }
}
